package com.hrhx.springboot.mysql.jpa;

import com.hrhx.springboot.domain.AutohomeBrand;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AutohomeBrandRepository extends JpaRepository<AutohomeBrand,Long> {
}
